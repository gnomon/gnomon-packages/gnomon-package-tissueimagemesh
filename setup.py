#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

short_descr = "Gnomon python plugins to convert 3D images into topological representations"
readme = open('README.md').read()


# find packages
pkgs = find_packages('src')

setup_kwds = dict(
    name='gnomon_package_tissueimagemesh',
    version="1.0.0",
    description=short_descr,
    long_description=readme,
    author="moi",
    author_email="moi@email.com",
    url='',
    license='LGPL-3.0',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    package_data={
        "": [
            "*.png",
            "*/*.png",
            "*/*/*.png",
            "*.json",
            "*/*.json",
            "*/*/*.json"
        ]
    },

    entry_points={
        'cellComplexFromCellImage': [
            'cellReconstructionGrifone = gnomon_package_tissueimagemesh.algorithm.cellComplexFromCellImage.cellReconstructionGrifone',
            # 'cellReconstructionDracoStem = gnomon_package_tissueimagemesh.algorithm.cellComplexFromCellImage.cellReconstructionDracoStem'
        ],
        'cellImageQuantification': [
            'surfaceMeshCellCurvature = gnomon_package_tissueimagemesh.algorithm.cellImageQuantification.surfaceMeshCellCurvature',
            'surfaceMeshCellLayer = gnomon_package_tissueimagemesh.algorithm.cellImageQuantification.surfaceMeshCellLayer',
            'surfaceMeshCellNormal = gnomon_package_tissueimagemesh.algorithm.cellImageQuantification.surfaceMeshCellNormal',
            'surfaceMeshCellProperty = gnomon_package_tissueimagemesh.algorithm.cellImageQuantification.surfaceMeshCellProperty',
            'wallMeshImageSignal = gnomon_package_tissueimagemesh.algorithm.cellImageQuantification.wallMeshImageSignal',
            'wallMeshSignalPolarities = gnomon_package_tissueimagemesh.algorithm.cellImageQuantification.wallMeshSignalPolarities',
        ],
        'meshFilter': [
            'surfaceCellMeshDecimation = gnomon_package_tissueimagemesh.algorithm.meshFilter.surfaceCellMeshDecimation',
            'surfaceCellMeshFlipOptimization = gnomon_package_tissueimagemesh.algorithm.meshFilter.surfaceCellMeshFlipOptimization',
            'surfaceMeshCellProjection = gnomon_package_tissueimagemesh.algorithm.meshFilter.surfaceMeshCellProjection'
        ],
        'meshFromImage': [
            'imageSurfaceMesh = gnomon_package_tissueimagemesh.algorithm.meshFromImage.imageSurfaceMesh',
            'segmentationSurfaceMesh = gnomon_package_tissueimagemesh.algorithm.meshFromImage.segmentationSurfaceMesh',
            'segmentationWallMesh = gnomon_package_tissueimagemesh.algorithm.meshFromImage.segmentationWallMesh',
            'surfaceMeshPyvista = gnomon_package_tissueimagemesh.algorithm.meshFromImage.surfaceMeshPyvista'
        ],
        'pointCloudQuantification': [
            'nucleiLayerEstimation = gnomon_package_tissueimagemesh.algorithm.pointCloudQuantification.nucleiLayerEstimation',
            'surfaceMeshProperty = gnomon_package_tissueimagemesh.algorithm.pointCloudQuantification.surfaceMeshProperty'
        ],
    },
    keywords='',
    
    test_suite='nose.collector',
    )

setup(**setup_kwds)
