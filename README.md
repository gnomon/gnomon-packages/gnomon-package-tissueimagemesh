# Gnomon plugin package : `Tissue Image Mesh`

This package contains Python plugins for the [Gnomon computational platform](https://gnomon.gitlabpages.inria.fr/gnomon/) to convert 3D images into topological representations.

## Installation

This package is published on the [gnomon anaconda channel](https://anaconda.org/gnomon/gnomon_package_tissueimagemesh).
To install it a **conda client** is needed such as [miniconda](https://docs.anaconda.com/miniconda/).

To install use the following command in your **gnomon** environment:

```shell
gnomon-utils package install gnomon_package_tissueimagemesh
```

Alternatively use:
```shell
conda install -c conda-forge -c gnomon -c mosaic -c morpheme -c dtk-forge6 gnomon_package_tissueimagemesh
```