import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestSurfaceMeshCurvature(unittest.TestCase):
    """Tests the surfaceMeshCellCurvature plugin.

    """

    @classmethod
    def setUpClass(cls) -> None:
        load_plugin_group("cellImageReader")
        load_plugin_group("cellImageQuantification")

    def setUp(self):
        self.filename = "test/resources/E37_SAM7_t00-P3_PI_seg.inr.gz"

        self.reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()

        self.cellImage = self.reader.cellImage()

        self.curvature = gnomon.core.cellImageQuantification_pluginFactory().create("surfaceMeshCellCurvature")

    def tearDown(self):
        self.reader.this.disown()
        self.curvature.this.disown()

    def test_surfaceMeshCurvature(self):
        self.curvature.setCellImage(self.cellImage)
        self.curvature.refreshParameters()
        self.curvature.run()

        assert self.curvature.outputCellImage() is not None
        cellImage = self.curvature.outputCellImage()[0]
        assert "mean_curvature" in cellImage.cellPropertyNames()

# test_gnomonCellComplexFromCellImage.py ends here.
