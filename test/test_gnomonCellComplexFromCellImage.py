import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonCellComplexFromCellImage(unittest.TestCase):
    """Tests the gnomonCellComplexFromCellImage class.

    """

    @classmethod
    def setUpClass(cls) -> None:
        load_plugin_group("cellImageReader")
        load_plugin_group("cellComplexFromCellImage")

    def setUp(self):
        self.filename = "test/resources/E37_SAM7_t00-P3_PI_seg.inr.gz"

        self.reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()

        self.cellImage = self.reader.cellImage()

        self.grifone = gnomon.core.cellComplexFromCellImage_pluginFactory().create("cellReconstructionGrifone")

    def tearDown(self):
        self.reader.this.disown()
        self.grifone.this.disown()

    def test_gnomonCellComplexFromCellImage_2d(self):
        self.grifone.setInput(self.cellImage)
        self.grifone.refreshParameters()
        self.grifone.setParameter("dimension", 2)
        self.grifone.run()
        assert self.grifone.output() is not None
        cellcomplex = self.grifone.output()[0]
        assert cellcomplex.elementCount(2) > 0

# test_gnomonCellComplexFromCellImage.py ends here.
