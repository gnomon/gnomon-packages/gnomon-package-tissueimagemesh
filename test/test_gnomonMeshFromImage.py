import unittest

import gnomon.core

from gnomon.utils import load_plugin_group


class TestGnomonMeshFromImage(unittest.TestCase):
    """Tests the gnomonMeshFromImage class.

    """

    @classmethod
    def setUpClass(cls) -> None:
        load_plugin_group("imageReader")
        load_plugin_group("meshFromImage")

    def setUp(self):
        self.filename = "test/resources/time_0_cut.inr"
        self.reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()
        self.image = self.reader.image()

        self.surface = gnomon.core.meshFromImage_pluginFactory().create("imageSurfaceMesh")
        self.surface.setImage(self.image)
        self.surface.refreshParameters()

        self.mesh = None

    def tearDown(self):
        self.surface.this.disown()

    def test_gnomonMeshFromImage_surface(self):
        self.surface.setParameter('gaussian_sigma', 2.0)
        self.surface.run()
        self.mesh = self.surface.output()[0]
        assert self.mesh is not None
        assert self.mesh.cellsCount(0) > 0
        assert self.mesh.cellsCount(2) > 0

#
# test_gnomonMultiChannelImage.py ends here.
