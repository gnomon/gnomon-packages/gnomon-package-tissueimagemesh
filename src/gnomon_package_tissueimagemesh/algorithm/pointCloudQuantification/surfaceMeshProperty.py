from copy import deepcopy

import numpy as np
import scipy.ndimage as nd
import pandas as pd

from dtkcore import d_inliststringlist

import gnomon.core
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, meshInput, pointCloudOutput, dataFrameOutput
from gnomon.core import gnomonAbstractPointCloudQuantification


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Nuclei Mesh Property")
@meshInput('surface_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class surfaceMeshProperty(gnomonAbstractPointCloudQuantification):
    """Transfer vertex attributes of a surface mesh to the nearest points.
    """

    def __init__(self):
        super().__init__()

        self.surface_topomesh = {}
        self.df = {}
        self.out_df = {}
        self.data_df = {}

        self._parameters = {}
        self._parameters['attribute_names'] = d_inliststringlist("Attributes", [""], [""], "List of vertex attributes to transfer from the surface mesh")

    def refreshParameters(self):
        if len(self.surface_topomesh)>0:
            surface_topomesh = list(self.surface_topomesh.values())[0]

            attribute_names = self['attribute_names']
            property_names = list(surface_topomesh.wisp_property_names(0))
            if len(property_names) == 0:
                property_names = [""]
            attribute_names = [p for p in attribute_names if p in property_names]
            if len(attribute_names) == 0:
                attribute_names = property_names
            self._parameters['attribute_names'].setValues(property_names)
            self._parameters['attribute_names'].setValue(attribute_names)

    def run(self):
        self.set_max_progress((2+len(self['attribute_names']))*len(self.df))
        self.data_df = {}
        self.out_df = {}

        for time in self.df.keys():
            self.set_progress_message(f"T {time} : Copying point cloud")
            df = self.df[time]

            surface_topomesh = self.surface_topomesh[time]
            out_df = deepcopy(df)
            self.increment_progress()

            self.set_progress_message(f"T {time} : Retrieving labels of surface vertices")
            cell_labels = df['label'].values
            if surface_topomesh.has_wisp_property('label', 0, is_computed=True):
                vertex_cells = surface_topomesh.wisp_property('label', 0).values(list(surface_topomesh.wisps(0)))
            else:
                cell_centers = df[['center_'+dim for dim in 'xyz']].values

                vertex_points = surface_topomesh.wisp_property('barycenter', 0).values(list(surface_topomesh.wisps(0)))
                vertex_cell_distances = np.linalg.norm(vertex_points[:, np.newaxis] - cell_centers[np.newaxis], axis=-1)
                vertex_cells = cell_labels[np.argmin(vertex_cell_distances, axis=-1)]

                surface_topomesh.update_wisp_property('label', 0, dict(zip(surface_topomesh.wisps(0), vertex_cells)))
                surface_topomesh.update_wisp_property('label', 0, dict(zip(surface_topomesh.wisps(0), vertex_cells%256)))
            self.increment_progress()

            for property_name in self['attribute_names']:
                self.set_progress_message(f"T {time} : Transfering {property_name} property on points")
                vertex_property = surface_topomesh.wisp_property(property_name, 0).values(list(surface_topomesh.wisps(0)))
                property_values = np.unique(vertex_property)
                property_is_binary = vertex_property.ndim == 1 and len(property_values) <= 2 and (0 in property_values or 1 in property_values)
                if property_is_binary:
                    out_df[property_name] = (nd.sum(vertex_property, vertex_cells, index=cell_labels) > 0).astype(int)
                else:
                    if vertex_property.ndim == 1:
                        out_df[property_name] = nd.mean(vertex_property, vertex_cells, index=cell_labels)
                    elif vertex_property.ndim == 2:
                        out_df[property_name] = list(np.transpose([
                            nd.mean(vertex_property[:, k], vertex_cells, index=cell_labels)
                            for k in range(vertex_property.shape[1])
                        ]))
                    elif vertex_property.ndim == 3:
                        out_df[property_name] = list(np.transpose([[
                            nd.mean(vertex_property[:, j, k], vertex_cells, index=cell_labels)
                            for k in range(vertex_property.shape[2])
                        ] for j in range(vertex_property.shape[1])], (2, 1, 0)))
                self.increment_progress()

            self.out_df[time] = out_df
            self.data_df[time] = deepcopy(out_df)
