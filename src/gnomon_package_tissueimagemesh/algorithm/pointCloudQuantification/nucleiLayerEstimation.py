from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_inliststring, d_int

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, imageInput, meshInput, pointCloudOutput, dataFrameOutput
from gnomon.core import gnomonAbstractPointCloudQuantification

from timagetk.components.spatial_image import SpatialImage
from timagetk_geometry.features.nuclei import nuclei_layer


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Nuclei L1 Estimation")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@meshInput('surface_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class nucleiLayerEstimation(gnomonAbstractPointCloudQuantification):
    """Estimate cell layer of nuclei represented as a point cloud.

    The function extracts a 2D surface to estimate the first layer
    of nuclei as the closest points to any of the surface vertices.
    The surface can either be computed from the point cloud itself
    or from a 3D image passed as input.
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.surface_topomesh = {}
        self.df = {}
        self.out_df = {}
        self.data_df = {}

        self._parameters = {}
        self._parameters['orientation'] = d_inliststring("Orientation", "upright", ["upright", "inverted"], "Whether nuclei were acquired with an upright or inverted microscope")
        self._parameters['surface_channel'] = d_inliststring("Surface channel", "", [""], "Image channel on which to extract the surface")
        self._parameters['threshold'] = d_int("Threshold", 10, 0, 255, "Intensity threshold for the surface extraction")

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]
            if len(img) == 1:
                if 'surface_channel' in self._parameters.keys():
                    del self._parameters['surface_channel']
            else:
                if 'surface_channel' not in self._parameters.keys():
                    self._parameters['surface_channel'] = d_inliststring("Image channel on which to extract the surface", "", [""])
                self._parameters['surface_channel'].setValues(list(img.keys()))
                self._parameters['surface_channel'].setValue(list(img.keys())[0])

            img = img[list(img.keys())[0]]

            if img.dtype == np.uint8:
                self._parameters['threshold'].setMax(255)
                self._parameters['threshold'].setValue(10)
            elif img.dtype == np.uint16:
                self._parameters['threshold'].setMax(65535)
                self._parameters['threshold'].setValue(1000)

    def run(self):
        self.set_max_progress(3*len(self.df))
        self.data_df = {}
        self.out_df = {}

        for time in self.df.keys():
            self.set_progress_message(f"T {time} : Copying point cloud")
            out_df = deepcopy(self.df[time])

            nuclei_points = out_df[['center_'+dim for dim in 'xyz']].values
            nuclei_positions = dict(zip(out_df['label'].values, nuclei_points))

            if (time in self.surface_topomesh) and self.surface_topomesh[time] is not None:
                surface_topomesh = self.surface_topomesh[time]
            else:
                surface_topomesh = None

            if (time in self.img) and self.img[time] is not None:
                img = self.img[time]
                if 'surface_channel' in self._parameters.keys():
                    img = img[self['surface_channel']]
                else:
                    img = list(img.values())[0]
                surface_mode = "image"
            else:
                shape = np.ceil(np.max(nuclei_points, axis=0)).astype(int)
                img = SpatialImage(np.zeros(shape), voxelsize=(1., 1., 1.))
                surface_mode = "density"
            self.increment_progress()

            self.set_progress_message(f"T {time} : Estimating nuclei layer")
            cell_layer, surface_topomesh = nuclei_layer(
                nuclei_positions,
                surface_topomesh=surface_topomesh,
                nuclei_img=img,
                upwards=(self['orientation'] == 'upright'),
                surface_mode=surface_mode,
                threshold=self['threshold'],
                return_topomesh=True
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating point cloud")
            out_df['layer'] = [cell_layer[l] if l in cell_layer else np.nan for l in out_df['label'].values]

            for i_dim, dim in enumerate(['x', 'y', 'z']):
                surface_topomesh.update_wisp_property('barycenter_' + dim, 0, surface_topomesh.wisp_property('barycenter', 0).values()[:, i_dim])
            self.increment_progress()

            self.out_df[time] = out_df
            self.data_df[time] = deepcopy(out_df)
