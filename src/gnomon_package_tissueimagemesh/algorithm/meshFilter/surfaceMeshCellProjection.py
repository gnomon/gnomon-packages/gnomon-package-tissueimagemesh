# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_bool, d_real, d_inliststring

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, meshInput, meshOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

from timagetk_geometry.image_surface.tissue_image_mesh import surface_vertex_normal_sampling_label_projection, surface_vertex_remove_isolated_labels


# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='1.0.0', coreversion='1.0.0', name="Surface Cell Projection")
@cellImageInput("seg_img", data_plugin="gnomonCellImageDataTissueImage")
@meshInput(attr='in_surface_topomesh', data_plugin='gnomonMeshDataPropertyTopomesh')
@meshOutput(attr='surface_topomesh', data_plugin='gnomonMeshDataPropertyTopomesh')
class surfaceMeshCellProjection(gnomon.core.gnomonAbstractMeshFilter):
    """Project cell labels on surface mesh using normals

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['sampling_depth'] = d_real("Sampling depth", 3., 0., 10., 1, "Depth up to which to sample the segmented image")
        self._parameters['method'] = d_inliststring("Method", "median", ["most", "first", "median"], "Method used to choose the selected label")
        self._parameters['remove_isolated_labels'] = d_bool("Remove isolated labels",True, "Whether to remove labels that have no identical neighbor")

        self.seg_img = {}
        self.in_surface_topomesh = {}
        self.surface_topomesh = {}

    def run(self):
        self.set_max_progress(2*len(self.in_surface_topomesh))
        self.surface_topomesh = {}
        for time in self.in_surface_topomesh.keys():
            surface_topomesh = deepcopy(self.in_surface_topomesh[time])
            # #}
            # implement the run method

            self.set_progress_message(f"T {time} : Projecting cell labels on the mesh")
            seg_img = self.seg_img[time]

            surface_vertex_normal_sampling_label_projection(
                surface_topomesh,
                seg_img,
                sampling_depth=self['sampling_depth'],
                method=self['method']
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Removing {'isolated' if self['remove_isolated_labels'] else 'background'} labels")
            surface_vertex_remove_isolated_labels(
                surface_topomesh,
                background_only=not self['remove_isolated_labels']
            )
            self.increment_progress()

            self.surface_topomesh[time] = surface_topomesh
