# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_bool, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import meshInput, meshOutput
# #}
# add your imports before the next gnomon tag

from timagetk_geometry.image_surface.tissue_image_mesh import surface_tissue_mesh_collapse_same_label_edges

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.extraction import contiguous_wisps_topomesh


# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='1.0.0', coreversion='1.0.0', name="Surface cell mesh decimation")
@meshInput(attr='in_surface_topomesh', data_plugin='gnomonMeshDataPropertyTopomesh')
@meshOutput(attr='surface_topomesh', data_plugin='gnomonMeshDataPropertyTopomesh')
class surfaceCellMeshDecimation(gnomon.core.gnomonAbstractMeshFilter):
    """Decimate tissue mesh to keep one vertex per cell label

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}

        self.in_surface_topomesh = {}
        self.surface_topomesh = {}

    def run(self):
        self.set_max_progress(2*len(self.in_surface_topomesh))
        self.surface_topomesh = {}
        for time in self.in_surface_topomesh.keys():
            self.set_progress_message(f"T {time} : Collapsing same label edges")
            in_surface_topomesh = self.in_surface_topomesh[time]
            # #}
            # implement the run method

            surface_topomesh = surface_tissue_mesh_collapse_same_label_edges(
                in_surface_topomesh
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating mesh")
            surface_topomesh = contiguous_wisps_topomesh(surface_topomesh)

            compute_topomesh_property(surface_topomesh, 'vertices', 1)
            compute_topomesh_property(surface_topomesh, 'length', 1)
            compute_topomesh_property(surface_topomesh, 'borders', 2)
            compute_topomesh_property(surface_topomesh, 'vertices', 2)
            compute_topomesh_property(surface_topomesh, 'oriented_vertices', 2)
            compute_topomesh_property(surface_topomesh, 'oriented_borders', 3)
            self.increment_progress()

            self.surface_topomesh[time] = surface_topomesh
