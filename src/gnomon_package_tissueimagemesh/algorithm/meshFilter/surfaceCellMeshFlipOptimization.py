# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_int, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, meshInput, meshOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np

from timagetk_geometry.image_surface.tissue_image_mesh import surface_tissue_mesh_image_edge_flip_optimization


# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='1.0.0', coreversion='1.0.0', name="Cell Mesh Flip Optimization")
@cellImageInput("seg_img", data_plugin="gnomonCellImageDataTissueImage")
@meshInput(attr='in_surface_topomesh', data_plugin='gnomonMeshDataPropertyTopomesh')
@meshOutput(attr='surface_topomesh', data_plugin='gnomonMeshDataPropertyTopomesh')
class surfaceCellMeshFlipOptimization(gnomon.core.gnomonAbstractMeshFilter):
    """Perform edge flips to get closer to the image cell ajacencies

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['iterations'] = d_int("Iterations", 3, 0, 5, "The number of iterations to perform in the edge flip process")
        self._parameters['regularization'] = d_real("Regularization Weight", 0, 0, 1, 2, "The weight associated to the regularization energy term")
        
        self.seg_img = {}
        self.in_surface_topomesh = {}
        self.surface_topomesh = {}

    def run(self):
        self.set_max_progress(len(self.in_surface_topomesh))
        self.surface_topomesh = {}
        for time in self.in_surface_topomesh.keys():
            surface_topomesh = deepcopy(self.in_surface_topomesh[time])
            # #}
            # implement the run method

            if self['iterations']>0:
                self.set_progress_message(f"T {time} : Performing edge flip optimization ({self['iterations']} iterations)")
                seg_img = self.seg_img[time]
                surface_topomesh = surface_tissue_mesh_image_edge_flip_optimization(
                    surface_topomesh,
                    seg_img,
                    iterations=self['iterations'],
                    regularization_weight=self['regularization']
                )
            self.increment_progress()

            self.surface_topomesh[time] = surface_topomesh
