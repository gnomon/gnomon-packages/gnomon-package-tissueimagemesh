import logging
from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_real, d_inliststring

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, meshInput, cellImageOutput, dataFrameOutput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk import TissueImage3D
from timagetk.features.pandas_tools import cells_to_dataframe

from timagetk_geometry.image_surface.tissue_image_mesh import tissue_image_surface_topomesh
from timagetk_geometry.features.tissue_mesh import compute_cell_curvature_features


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Surface Mesh Curvature")
@meshInput('surface_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class surfaceMeshCellCurvature(gnomonAbstractCellImageQuantification):
    """
    Compute local curvature properties of surface cells.

    The method estimates the principal curvatures on a triangle mesh of the
    tissue surface an projects the information back on the nearest cells.

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.surface_topomesh = {}

        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['orientation'] = d_inliststring('Orientation', "up", ["up", "down"], "Whether to keep upper or lower part")
        self._parameters['resampling_voxelsize'] = d_real("Resampling voxelsize", 1., 0.1, 5., 1, "Voxelsize to use for the resampling allowing to estimate the surface cell triangulation")
        self._parameters['down_facing_threshold'] = d_real("Down threshold", 0., -1, 1., 2, "Angle threshold under with the surface (pointing downwards) will be removed")

        self._parameter_groups = {}
        for parameter_name in ['orientation','resampling_voxelsize', 'down_facing_threshold']:
            self._parameter_groups[parameter_name] = 'surface_extraction'

    def __del__(self):
        pass

    def run(self):
        self.set_max_progress(3*len(self.tissue))
        self.out_tissue = {}
        self.df = {}

        if len(self.tissue) == 0:
            logging.error("Impossible to quantify! No segmented image provided")
            return

        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : Copying cell image")
            tissue = self.tissue[time]

            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            self.set_progress_message(f"T {time} : Retrieving tissue surface mesh")
            if time in self.surface_topomesh:
                surface_topomesh = self.surface_topomesh[time]
            else:
                surface_topomesh = tissue_image_surface_topomesh(
                    out_tissue,
                    resampling_voxelsize=self['resampling_voxelsize'],
                    surface_matching='cell',
                    orientation=(1 if self['orientation'] == "up" else -1),
                    down_facing_threshold=self['down_facing_threshold']
                )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Computing cell curvatures")
            compute_cell_curvature_features(out_tissue, surface_topomesh=surface_topomesh)
            self.increment_progress()

            self.out_tissue[time] = out_tissue
            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=out_tissue.cell_ids())
