from dtkcore import d_inliststring, d_bool

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, meshInput
from gnomon.utils.decorators import cellImageOutput, dataFrameOutput
from gnomon.core import gnomonAbstractCellImageQuantification

import logging
from copy import deepcopy

import numpy as np

from timagetk import TissueImage3D
from timagetk.features.pandas_tools import cells_to_dataframe

from timagetk_geometry.image_surface.tissue_image_mesh import surface_vertex_normal_sampling_label_projection
from timagetk_geometry.image_surface.tissue_image_mesh import surface_vertex_remove_isolated_labels
from timagetk_geometry.features.tissue_mesh import compute_cell_normal_vector_feature


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Surface Mesh Normal")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@meshInput('surface_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class surfaceMeshCellNormal(gnomonAbstractCellImageQuantification):
    """Compute local normal vectors of all cells.

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.surface_topomesh = {}

        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['label_projection'] = d_bool('Label Projection', True, "Whether to force projection of cell labels on the mesh")

    def __del__(self):
        pass

    def run(self):
        self.set_max_progress(3*len(self.tissue))
        self.out_tissue = {}
        self.df = {}

        if len(self.tissue) == 0:
            logging.error("Impossible to quantify! No segmented image provided")
            return

        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : Copying cell image")
            tissue = self.tissue[time]

            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            self.set_progress_message(f"T {time} : Projecting cells on surface mesh")
            surface_topomesh = self.surface_topomesh[time]

            if self['label_projection'] or (not surface_topomesh.has_wisp_property('label', 0, is_computed=True)):
                surface_vertex_normal_sampling_label_projection(surface_topomesh, out_tissue)
                surface_vertex_remove_isolated_labels(surface_topomesh, background_only=True)
            self.increment_progress()

            self.set_progress_message(f"T {time} : Computing cell normal vectors")
            compute_cell_normal_vector_feature(out_tissue, surface_topomesh=surface_topomesh)
            self.increment_progress()

            self.out_tissue[time] = out_tissue
            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=out_tissue.cell_ids())
