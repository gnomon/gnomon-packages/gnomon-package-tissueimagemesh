import logging
from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_real, d_inliststring, d_inliststringlist

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, imageInput, meshInput, cellImageOutput, dataFrameOutput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk import TissueImage3D
from timagetk.features.pandas_tools import cells_to_dataframe

from timagetk_geometry.signal_quantification.wall_mesh import quantify_wall_topomesh_signal_intensity


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Wall Mesh Signal")
@meshInput('wall_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class wallMeshImageSignal(gnomonAbstractCellImageQuantification):
    """Measure cell-wall image signal intensity using a triangle mesh.

    The method computes a value of signal for each cell wall represented in a
    wall triangle mesh by averaging the image intensity at the level of the
    mesh vertices.

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.img = {}
        self.wall_topomesh = {}

        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['channels'] = d_inliststringlist("Channels", [""],[""], "Channels on which to quantify signal intensity at the cell walls")
        self._parameters['gaussian_sigma'] = d_real("Sigma", 0.5, 0., 50., 2, "Standard deviation of the Gaussian kernel used to smooth signal")

        self._parameter_groups = {}

    def __del__(self):
        pass

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]

            if len(img.channel_names) == 1:
                if 'channels' in self._parameters.keys():
                    del self._parameters['channels']
            else:
                if not 'channels' in self._parameters.keys():
                    self._parameters['channels'] = d_inliststringlist("channels", [""], [""], "Channels on which to quantify signal intensity at the cell walls")
                self._parameters['channels'].setValues(img.channel_names)
                self._parameters['channels'].setValue(img.channel_names)

    def run(self):
        self.set_max_progress(3*len(self.tissue))
        self.out_tissue = {}
        self.df = {}

        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : Copying cell image")
            if not time in self.img.keys():
                logging.error("Impossible to quantify! No signal image provided")
                self.df = {}
                return
            if not time in self.wall_topomesh.keys():
                logging.error("Impossible to quantify! No wall mesh provided")
                self.df = {}
                return
            tissue = self.tissue[time]

            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            img = self.img[time]
            wall_topomesh = self.wall_topomesh[time]
            self.set_progress_message(f"T {time} : Quantifying wall signals")

            wall_signals = quantify_wall_topomesh_signal_intensity(
                signal_img=img,
                wall_topomesh=wall_topomesh,
                wall_sigma=self['gaussian_sigma']
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating wall properties")
            for channel in wall_signals:
                signal_name = channel if channel != "" else "image_signal"
                if ('channels' not in self._parameters) or (signal_name in self['channels']):
                    out_tissue.walls.set_feature(signal_name, wall_signals[channel])
            self.increment_progress()

            self.out_tissue[time] = out_tissue
            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=out_tissue.cell_ids())
