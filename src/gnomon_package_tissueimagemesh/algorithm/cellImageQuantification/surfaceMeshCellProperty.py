from copy import deepcopy

import numpy as np
import scipy.ndimage as nd
import pandas as pd

from dtkcore import d_inliststringlist, d_int

import gnomon.core
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, meshInput, cellImageOutput, dataFrameOutput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk import TissueImage3D
from timagetk.features.pandas_tools import cells_to_dataframe

from timagetk_geometry.features.cells import compute_cell_surface_center_feature

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Surface Mesh Property")
@meshInput('surface_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput('out_tissue', data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class surfaceMeshCellProperty(gnomonAbstractCellImageQuantification):
    """Transfer vertex attributes of a surface mesh to the nearest cells.
    """

    def __init__(self):
        super().__init__()

        self.surface_topomesh = {}
        self.tissue = {}
        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['attribute_names'] = d_inliststringlist("Attributes", [""], [""], "List of vertex attributes to transfer from the surface mesh")
        self._parameters['label_threshold'] = d_int("Max Label Number", 64, 0, 10000, "Maximal number of different values for an attribute to be considered a label")

    def refreshParameters(self):
        if len(self.surface_topomesh)>0:
            surface_topomesh = list(self.surface_topomesh.values())[0]

            attribute_names = self['attribute_names']
            property_names = list(surface_topomesh.wisp_property_names(0))
            property_names = [p for p in property_names if surface_topomesh.has_wisp_property(p, 0, is_computed=True)]
            property_names = [p for p in property_names if surface_topomesh.wisp_property(p, 0).values().dtype != np.dtype('O')]
            if len(property_names) == 0:
                property_names = [""]
            attribute_names = [p for p in attribute_names if p in property_names]
            if len(attribute_names) == 0:
                attribute_names = property_names
            self._parameters['attribute_names'].setValues(property_names)
            self._parameters['attribute_names'].setValue(attribute_names)

    def run(self):
        self.set_max_progress((2+len(self['attribute_names']))*len(self.tissue))
        self.out_tissue = {}
        self.df = {}

        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : Copying cell image")
            tissue = self.tissue[time]

            surface_topomesh = self.surface_topomesh[time]

            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            self.set_progress_message(f"T {time} : Retrieving cell labels of surface vertices")
            cell_labels = np.array(out_tissue.cell_ids())
            if surface_topomesh.has_wisp_property('label', 0, is_computed=True):
                vertex_cells = surface_topomesh.wisp_property('label', 0).values(list(surface_topomesh.wisps(0)))
            else:
                if 'surface_center' in out_tissue.cells.feature_names():
                    cell_surface_centers = out_tissue.cells.feature('surface_center')
                else:
                    cell_surface_centers = compute_cell_surface_center_feature(out_tissue)
                cell_centers = np.array([cell_surface_centers[c] for c in cell_labels])

                vertex_points = surface_topomesh.wisp_property('barycenter', 0).values(list(surface_topomesh.wisps(0)))
                vertex_cell_distances = np.linalg.norm(vertex_points[:, np.newaxis] - cell_centers[np.newaxis], axis=-1)
                vertex_cells = cell_labels[np.argmin(vertex_cell_distances, axis=-1)]

                surface_topomesh.update_wisp_property('label', 0, dict(zip(surface_topomesh.wisps(0), vertex_cells)))
                surface_topomesh.update_wisp_property('display_label', 0, dict(zip(surface_topomesh.wisps(0), vertex_cells%256)))
            self.increment_progress()

            surface_cells = np.unique(vertex_cells)
            for property_name in self['attribute_names']:
                self.set_progress_message(f"T {time} : Transfering {property_name} property on cells")
                vertex_property = surface_topomesh.wisp_property(property_name, 0).values(list(surface_topomesh.wisps(0)))
                property_values = np.unique(vertex_property)
                property_is_binary = vertex_property.ndim == 1 and len(property_values) <= 2 and (0 in property_values or 1 in property_values)
                property_is_label = vertex_property.ndim == 1 and len(property_values) <= self['label_threshold']
                cell_property = None
                if property_is_binary:
                    cell_property = (nd.sum(vertex_property, vertex_cells, index=surface_cells) > 0).astype(int)
                elif property_is_label:
                    def most_represented(array):
                        values, counts = np.unique(array, return_counts=True)
                        return values[np.argmax(counts)]
                    dtype = property_values.dtype
                    default = np.iinfo(dtype).min if np.issubdtype(dtype, np.integer) else np.nan
                    cell_property = nd.labeled_comprehension(
                        vertex_property, vertex_cells, index=surface_cells,
                        func=most_represented, out_dtype=dtype, default=default
                    )
                else:
                    if vertex_property.ndim == 1:
                        cell_property = nd.mean(vertex_property, vertex_cells, index=surface_cells)
                    elif vertex_property.ndim == 2:
                        cell_property = list(np.transpose([
                            nd.mean(vertex_property[:, k], vertex_cells, index=surface_cells)
                            for k in range(vertex_property.shape[1])
                        ]))
                    elif vertex_property.ndim == 3:
                        cell_property = list(np.transpose([[
                            nd.mean(vertex_property[:, j, k], vertex_cells, index=surface_cells)
                            for k in range(vertex_property.shape[2])
                        ] for j in range(vertex_property.shape[1])], (2, 1, 0)))
                if cell_property is not None:
                    out_tissue.cells.set_feature(property_name, dict(zip(surface_cells, cell_property)))
                self.increment_progress()

            self.out_tissue[time] = out_tissue

            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=out_tissue.cell_ids())
