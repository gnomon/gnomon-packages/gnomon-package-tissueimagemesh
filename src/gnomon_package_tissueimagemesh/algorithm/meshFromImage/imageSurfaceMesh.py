import numpy as np

from skimage.filters.thresholding import threshold_otsu

from dtkcore import d_int, d_inliststring, d_real

import gnomon.core
from gnomon.core import gnomonAbstractMeshFromImage

from gnomon.utils import algorithmPlugin

from gnomon.utils.decorators import imageInput, meshOutput

from timagetk_geometry.image_surface.image_mesh import image_surface_topomesh, up_facing_surface_topomesh

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.creation import triangle_topomesh
from cellcomplex.utils import array_dict


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Surface Mesh")
@imageInput("img", data_plugin='gnomonImageDataMultiChannelImage')
@meshOutput("topomesh", data_plugin="gnomonMeshDataPropertyTopomesh")
class imageSurfaceMesh(gnomonAbstractMeshFromImage):
    """Reconstruct the surface on an intensity image as a triangular mesh.

    The selected channel of the input image is first smoothed by a Gaussian
    filter of standard deviation gaussian_sigma and the result is thresholded
    by the value of the threshold parameter. The obtained binary mask is
    meshed using a Marching Cubes algorithm and decimated to reach the target
    edge_length. Only the largest connected component o the top or bottom part
    of the mesh is kept depending on the orientation parameter, after removing
    the parts beyond a given threshold of vertical orientation of the surface
    normal vectors.
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.topomesh = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring('Channel', "", [""], "Channel on which to extract the mesh")
        self._parameters['threshold'] = d_int('Threshold', 20, 0, 255, "Intensity threshold")
        self._parameters['resampling_voxelsize'] = d_real("Resampling voxelsize", 0, 0, 1, 2, "Cubic voxelsize in which to resample the image before mesh extraction")

        self._parameters['gaussian_sigma'] = d_real('Sigma', 2, 0., 20., 2, "Standard deviation of the Gaussian kernel used to smooth signal")
        self._parameters['orientation'] = d_inliststring('Orientation', "up", ["up", "down"], "Whether to keep upper or lower part")
        self._parameters['down_threshold'] = d_real('Orientation threshold', 0, -1, 1, 1, "The z coordinate below which a normal vector is considered to face downwards")
        self._parameters['edge_length'] = d_real('Edge length', 10, 0, 20, 1, "Maximal length of mesh edges")

        self._parameter_groups = {}
        for parameter_name in ['resampling_voxelsize', 'gaussian_sigma']:
            self._parameter_groups[parameter_name] = 'image_preprocessing'
        for parameter_name in ['orientation', 'down_threshold']:
            self._parameter_groups[parameter_name] = 'surface_cutting'

    def refreshParameters(self):
        if len(self.img)>0:
            img_dict = list(self.img.values())[0]
            if len(img_dict.channel_names) == 1:
                del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring('Channel', "", [""], "Channel on which to extract the mesh")
                channel = self['channel']
                self._parameters['channel'].setValues(list(img_dict.channel_names))
                if channel in img_dict.channel_names:
                    self._parameters['channel'].setValue(channel)
                else:
                    self._parameters['channel'].setValue(list(img_dict.channel_names)[0])

            if len(img_dict) == 1:
                img = list(img_dict.values())[0]
            else:
                img = img_dict[self['channel']]

            if img.dtype == np.uint8:
                self._parameters['threshold'].setMax(255)
            elif img.dtype == np.uint16:
                self._parameters['threshold'].setMax(65535)
            self._parameters['threshold'].setValue(int(np.round(1.2*threshold_otsu(img.get_array()))))

            self._parameters['resampling_voxelsize'].setMin(0)
            self._parameters['resampling_voxelsize'].setMax(10*np.max(img.voxelsize))
            self._parameters['resampling_voxelsize'].setValue(4*np.mean(img.voxelsize))

    def run(self):
        self.set_max_progress(3*len(self.img))
        self.topomesh = {}

        for time in self.img.keys():
            self.set_progress_message(f"T {time} : Extracting image surface mesh")
            if 'channel' in self._parameters.keys():
                img = self.img[time].get_channel(self['channel'])
            else:
                img = self.img[time].get_channel(self.img[time].channel_names[0])

            resampling_voxelsize = None if self['resampling_voxelsize'] == 0 else self['resampling_voxelsize']
            topomesh = image_surface_topomesh(
                img,
                resampling_voxelsize=resampling_voxelsize,
                gaussian_sigma=self['gaussian_sigma'],
                threshold=self['threshold'],
                target_length=self['edge_length']
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Keeping only {self['orientation']}-facing surface")
            topomesh = up_facing_surface_topomesh(
                topomesh,
                upwards=self['orientation']=="up",
                down_facing_threshold=self['down_threshold'],
                normal_method='orientation',
                connected=True
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating mesh")
            compute_topomesh_property(topomesh, 'vertices', 2)

            points = topomesh.wisp_property('barycenter', 0).values(list(topomesh.wisps(0)))
            point_mapping = array_dict(dict(zip(list(topomesh.wisps(0)), np.arange(len(points)))))
            triangles = topomesh.wisp_property('vertices', 2).values(list(topomesh.wisps(2)))
            triangles = point_mapping.values(triangles)

            topomesh = triangle_topomesh(triangles, points)

            for degree in [1, 2, 3]:
                compute_topomesh_property(topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))
            self.increment_progress()

            self.topomesh[time] = topomesh
