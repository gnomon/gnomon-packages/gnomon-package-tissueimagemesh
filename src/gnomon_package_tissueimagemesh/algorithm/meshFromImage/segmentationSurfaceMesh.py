import numpy as np

from skimage.filters.thresholding import threshold_otsu

from dtkcore import d_bool, d_int, d_inliststring, d_real

import gnomon.core
from gnomon.core import gnomonAbstractMeshFromImage

from gnomon.utils import algorithmPlugin

from gnomon.utils.decorators import cellImageInput, meshOutput

from timagetk_geometry.image_surface.tissue_image_mesh import tissue_image_surface_topomesh

from cellcomplex.property_topomesh.analysis import compute_topomesh_property


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Segmentation Surface Mesh")
@cellImageInput("seg_img", data_plugin='gnomonCellImageDataTissueImage')
@meshOutput("topomesh", data_plugin="gnomonMeshDataPropertyTopomesh")
class segmentationSurfaceMesh(gnomonAbstractMeshFromImage):
    """Reconstruct the surface on an segmented image as a triangular mesh.
    """

    def __init__(self):
        super().__init__()

        self.seg_img = {}
        self.topomesh = {}

        self._parameters = {}
        self._parameters['resampling_voxelsize'] = d_real("Resampling voxelsize", 0, 0, 1, 2, "Cubic voxelsize in which to resample the image before mesh extraction")
        self._parameters['orientation'] = d_inliststring('Orientation', "up", ["up", "down"], "Whether to keep upper or lower part")
        self._parameters['down_facing_threshold'] = d_real("Normal z threshold", 0, -1, 1, 2, "The z coordinate below which a normal vector is considered to face downwards")
        self._parameters['fill_holes'] = d_bool("Fill holes", False, "Whether to fill holes in the segmented image to generate the surface mesh")
        self._parameters['padding'] = d_bool("Padding", False, "Whether to pad the image with background before generating the surface mesh")
        self._parameters['gaussian_sigma'] = d_real("Gaussian sigma", 0., 0., 10., 2, "Standard deviation of the gaussian kernel used to smooth the segmented image")

        self._parameter_groups = {}

    def refreshParameters(self):
        if len(self.seg_img)>0:
            seg_img = list(self.seg_img.values())[0]

            self._parameters['resampling_voxelsize'].setMin(0)
            self._parameters['resampling_voxelsize'].setMax(10*np.max(seg_img.voxelsize))
            self._parameters['resampling_voxelsize'].setValue(4*np.mean(seg_img.voxelsize))

    def run(self):
        self.set_max_progress(2*len(self.seg_img))
        self.topomesh = {}

        for time in self.seg_img.keys():
            self.set_progress_message(f"T {time} : Extracting segmentation surface mesh")
            seg_img = self.seg_img[time]

            resampling_voxelsize = None if self['resampling_voxelsize'] == 0 else self['resampling_voxelsize']
            topomesh = tissue_image_surface_topomesh(
                seg_img=seg_img,
                resampling_voxelsize=resampling_voxelsize,
                orientation=(1 if self['orientation']=="up" else -1),
                surface_matching='cell',
                padding=self['padding'],
                gaussian_sigma=self['gaussian_sigma'],
                down_facing_threshold=self['down_facing_threshold'],
                fill_holes=self['fill_holes']
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating mesh")
            for degree in [1, 2, 3]:
                compute_topomesh_property(topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))
            self.increment_progress()

            self.topomesh[time] = topomesh
