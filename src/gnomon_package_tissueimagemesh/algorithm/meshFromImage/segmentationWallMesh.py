import numpy as np


from dtkcore import d_bool, d_int, d_inliststringlist, d_real

import gnomon.core
from gnomon.core import gnomonAbstractMeshFromImage

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, meshOutput

from timagetk_geometry.image_surface.wall_mesh import tissue_image_wall_topomesh

from cellcomplex.property_topomesh.analysis import compute_topomesh_property


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Wall Meshes")
@cellImageInput("seg_img", data_plugin='gnomonCellImageDataTissueImage')
@meshOutput("topomesh", data_plugin="gnomonMeshDataPropertyTopomesh")
class segmentationWallMesh(gnomonAbstractMeshFromImage):
    """Extract the cell walls of a segmented image as triangular meshes.
    """

    def __init__(self):
        super().__init__()

        self.seg_img = {}
        self.topomesh = {}

        self._parameters = {}
        self._parameters['resampling_voxelsize'] = d_real("Resampling voxelsize", 0, 0, 1, 2, "Cubic voxelsize in which to resample the image before mesh extraction")
        self._parameters['smoothing'] = d_bool("Smoothing", True, "Whether to smooth and decimate the wall meshes")
        self._parameters['wall_types'] = d_inliststringlist('Walls', ["anticlinal_L1"], ["anticlinal_L1", "epidermis_L1", "all"], "Which cell walls to extract in the resulting mesh")
        self._parameters['edge_length'] = d_real('Edge length', 1, 0, 10, 1, "Target length of mesh edges")

        self._parameter_groups = {}

    def refreshParameters(self):
        if len(self.seg_img)>0:
            seg_img = list(self.seg_img.values())[0]

            self._parameters['resampling_voxelsize'].setMin(0)
            self._parameters['resampling_voxelsize'].setMax(10*np.max(seg_img.voxelsize))
            self._parameters['resampling_voxelsize'].setValue(4*np.mean(seg_img.voxelsize))

    def run(self):
        self.set_max_progress(2*len(self.seg_img))
        self.topomesh = {}

        for time in self.seg_img.keys():
            self.set_progress_message(f"T {time} : Extracting segmentation wall meshes")
            seg_img = self.seg_img[time]

            resampling_voxelsize = None if self['resampling_voxelsize'] == 0 else self['resampling_voxelsize']
            topomesh = tissue_image_wall_topomesh(
                tissue=seg_img,
                resampling_voxelsize=resampling_voxelsize,
                wall_types=self['wall_types'],
                smoothing=self['smoothing'],
                target_edge_length=self['edge_length']
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating mesh")
            for degree in [1, 2, 3]:
                compute_topomesh_property(topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))
            self.increment_progress()

            self.topomesh[time] = topomesh
