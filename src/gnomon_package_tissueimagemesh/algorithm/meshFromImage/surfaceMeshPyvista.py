import numpy as np
import scipy.ndimage as nd
import pyvista as pv

from skimage.filters.thresholding import threshold_otsu

from dtkcore import d_int, d_inliststring, d_real, d_bool

import gnomon.core
from gnomon.core import gnomonAbstractMeshFromImage

from gnomon.utils import algorithmPlugin

from gnomon.utils.decorators import imageInput, cellImageInput, meshOutput

from timagetk import SpatialImage
from timagetk.algorithms.resample import isometric_resampling

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.creation import triangle_topomesh


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="PyVista Surface Mesh")
@imageInput("img", data_plugin='gnomonImageDataMultiChannelImage')
@cellImageInput("seg_img", data_plugin='gnomonCellImageDataTissueImage')
@meshOutput("topomesh", data_plugin="gnomonMeshDataPropertyTopomesh")
class surfaceMeshPyvista(gnomonAbstractMeshFromImage):
    """Extract the surface on an image as a triangular mesh.
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.seg_img = {}
        self.topomesh = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring('Channel', "", [""], "Channel on which to extract the mesh")
        self._parameters['threshold'] = d_int('Threshold', 20, 0, 255, "Intensity threshold")
        self._parameters['resampling_voxelsize'] = d_real("Resampling voxelsize", 0, 0, 1, 2, "Cubic voxelsize in which to resample the image before mesh extraction")
        self._parameters['padding'] = d_bool("Padding", False, "Whether to pad the image with background before generating the surface mesh")

        self._parameters['gaussian_sigma'] = d_real('Sigma', 2, 0., 20., 2, "Standard deviation of the Gaussian kernel used to smooth signal")
        self._parameters['orientation'] = d_inliststring('Orientation', "up", ["up", "down"], "Whether to keep upper or lower part")
        self._parameters['down_threshold'] = d_real('Orientation threshold', 0, -1, 1, 1, "The z coordinate below which a normal vector is considered to face downwards")
        self._parameters['edge_length'] = d_real('Edge length', 10, 0, 20, 1, "Maximal length of mesh edges")

        self._parameter_groups = {}
        for parameter_name in ['resampling_voxelsize', 'gaussian_sigma', 'padding']:
            self._parameter_groups[parameter_name] = 'image_preprocessing'
        for parameter_name in ['orientation', 'down_threshold']:
            self._parameter_groups[parameter_name] = 'surface_cutting'

    def refreshParameters(self):
        if len(self.seg_img)>0:
            seg_img = list(self.seg_img.values())[0]
            self._parameters['resampling_voxelsize'].setMin(0)
            self._parameters['resampling_voxelsize'].setMax(10*np.max(seg_img.voxelsize))
            self._parameters['resampling_voxelsize'].setValue(4*np.mean(seg_img.voxelsize))

        elif len(self.img)>0:
            img_dict = list(self.img.values())[0]
            if len(img_dict.channel_names) == 1:
                del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring('Channel', "", [""], "Channel on which to extract the mesh")
                channel = self['channel']
                self._parameters['channel'].setValues(list(img_dict.channel_names))
                if channel in img_dict.channel_names:
                    self._parameters['channel'].setValue(channel)
                else:
                    self._parameters['channel'].setValue(list(img_dict.channel_names)[0])

            if len(img_dict) == 1:
                img = list(img_dict.values())[0]
            else:
                img = img_dict[self['channel']]

            if img.dtype == np.uint8:
                self._parameters['threshold'].setMax(255)
            elif img.dtype == np.uint16:
                self._parameters['threshold'].setMax(65535)
            self._parameters['threshold'].setValue(int(np.round(0.5*threshold_otsu(img.get_array()))))

            self._parameters['resampling_voxelsize'].setMin(0)
            self._parameters['resampling_voxelsize'].setMax(10*np.max(img.voxelsize))
            self._parameters['resampling_voxelsize'].setValue(4*np.mean(img.voxelsize))

    def run(self):
        self.set_max_progress(5*len(self.img))
        self.topomesh = {}

        if len(self.seg_img)>0:
            times = list(self.seg_img.keys())
        else:
            times = list(self.img.keys())

        for time in times:
            self.set_progress_message(f"T {time} : Extracting image surface mesh")
            if time in self.seg_img:
                img = (255*(self.seg_img[time] > 1)).astype(np.uint16)
            else:
                if 'channel' in self._parameters.keys():
                    img = self.img[time].get_channel(self['channel'])
                else:
                    img = self.img[time].get_channel(self.img[time].channel_names[0])

            resampling_voxelsize = None if self['resampling_voxelsize'] == 0 else self['resampling_voxelsize']

            self.set_progress_message(f"T {time} : Formatting image")
            if self['padding']:
                pad_shape = np.transpose(np.tile(np.array(img.shape)//2, (2, 1)))
                padded_img = np.pad(img.get_array(), pad_shape, mode='constant')
                padded_img = SpatialImage(padded_img, voxelsize=img.voxelsize)
            else:
                padded_img = SpatialImage(img.get_array(), voxelsize=img.voxelsize)

            if resampling_voxelsize is None:
                resampling_voxelsize = np.min(img.voxelsize)
            resampled_img = isometric_resampling(padded_img, value=float(np.min(resampling_voxelsize)))

            smooth_img = nd.gaussian_filter(resampled_img.get_array(), self['gaussian_sigma']/np.array(resampled_img.voxelsize))
            if time in self.seg_img:
                binary_img = (smooth_img > 128).astype(np.uint8)
            else:
                binary_img = (smooth_img > self['threshold']).astype(np.uint8)
            binary_img = nd.binary_fill_holes(binary_img).astype(np.uint8)
            self.increment_progress()

            self.set_progress_message(f"T {time} : Extracting image surface")
            image_data = pv.ImageData(dimensions=resampled_img.shape[::-1], spacing=resampled_img.voxelsize[::-1])
            image_data.point_data['mask'] = binary_img.flatten().astype(np.float32)
            image_mesh = image_data.contour(isosurfaces=[0.5], scalars='mask', method='marching_cubes')

            if self['padding']:
                image_mesh.points -= 0.25*(np.array(resampled_img.voxelsize)*np.array(binary_img.shape))[::-1]

            image_mesh.smooth_taubin(
                feature_smoothing=False,
                pass_band=0.05,
                n_iter=20,
                inplace=True
            )

            decimation_reduction = 1 - np.power(np.min(resampling_voxelsize)/self['edge_length'], 2)
            image_mesh.decimate(
                target_reduction=decimation_reduction,
                inplace=True
            )

            image_mesh.smooth_taubin(
                feature_smoothing=False,
                pass_band=0.05,
                n_iter=20,
                inplace=True
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Keeping only {self['orientation']}-facing surface")
            image_mesh.compute_normals(
                inplace=True
            )

            point_normals = image_mesh.point_data['Normals']
            orientation = 1 - 2*(self['orientation']=="down")
            point_up_facing = orientation*point_normals[:, 2] > orientation*self['down_threshold']
            up_facing_points = np.arange(image_mesh.n_points)[point_up_facing]

            image_mesh = image_mesh.extract_points(
                up_facing_points,
                include_cells=True,
            ).extract_largest().extract_surface()
            self.increment_progress()

            self.set_progress_message(f"T {time} : Creating triangle mesh")
            mesh_points = image_mesh.points
            mesh_triangles = image_mesh.faces.reshape((-1, 4))[:, 1:]
            topomesh = triangle_topomesh(mesh_triangles, mesh_points)
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating mesh properties")
            for degree in [1, 2, 3]:
                compute_topomesh_property(topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))
            self.increment_progress()

            self.topomesh[time] = topomesh
