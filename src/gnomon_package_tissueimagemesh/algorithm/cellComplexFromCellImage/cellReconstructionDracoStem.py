from dtkcore import d_bool, d_int, d_real

import gnomon.core
import numpy as np

from gnomon.core import gnomonAbstractCellComplexFromCellImage

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.extraction import clean_topomesh
from timagetk_geometry.draco import DracoMesh

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellComplexOutput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="DRACO Reconstruction")
@cellImageInput('tissue', data_plugin="gnomonCellImageDataTissueImage")
@cellComplexOutput('topomesh', data_plugin="gnomonCellComplexDataPropertyTopomesh")
class cellReconstructionDracoStem(gnomonAbstractCellComplexFromCellImage):
    """Reconstruct a cell complex using Draco-Stem

    Note
    ----

    The documentation is not complete

    """

    def __init__(self):
        super().__init__()
        self.tissue = {}
        self.topomesh = {}
        self.adjacency_series = {}

        self._parameters = {}
        self._parameters['dimension'] = d_int('Dimension', 2, 2, 3, "Dimension of the adjacency complex")
        self._parameters['iterations'] = d_int('Iterations', 2, 0, 10, "Number of iterations for the topological optimization of adjacency complex (if dimension = 3)")
        self._parameters['maximal_distance'] = d_real('Max distance', 15., 0, 100, 1, "Maximal distance between two cells in the adjacency complex (if dimension = 3)")
        self._parameters['triangulate'] = d_bool('Triangulate', False, "Whether to triangulate the interfaces of the resulting cell complex")
        self._parameters['edge_length'] = d_real('Edge length', 10, 0, 20, 1, "Maximal length of triangle edges (if triangulate)")

        self.__doc__ = DracoMesh.__doc__

    def adjacencyComplex(self):
        return self.adjacency_series

    def run(self):
        self.set_max_progress(6*len(self.tissue))
        self.topomesh = {}

        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : Extracting image cell adjacencies")
            tissue = self.tissue[time]

            draco = DracoMesh(tissue)
            self.increment_progress()

            if self["dimension"] == 3:
                self.set_progress_message(f"T {time} : Computing Delaunay tetrahedrization")
                draco.delaunay_adjacency_complex(surface_cleaning_criteria = ['sliver', 'distance'], maximal_distance=self['maximal_distance'])
                self.increment_progress()

                self.set_progress_message(f"T {time} : Optimizing adjacency complex ({self['iterations']} iterations)")
                draco.adjacency_complex_optimization(n_iterations=self['iterations'])
            elif self["dimension"] == 2:
                self.set_progress_message(f"T {time} : Constructing layer adjacency complex")
                self.increment_progress()
                draco.layer_adjacency_complex()
            self.adjacency_series[time] = draco.triangulation_topomesh
            self.increment_progress()

            self.set_progress_message(f"T {time} : Reconstructing {'and triangulating ' if self['triangulate'] else ''}dual cell complex")
            if self['triangulate']:
                if self["dimension"] == 3:
                    triangular = ['star', 'remeshed', 'projected', 'regular', 'flat']
                elif self["dimension"] == 2:
                    triangular = ['star', 'remeshed', 'straight', 'regular']
            else:
                triangular = []
            topomesh = draco.dual_reconstruction(reconstruction_triangulation = triangular,
                                                 adjacency_complex_degree = self['dimension'],
                                                 maximal_edge_length = self['edge_length']) # SEGFAULT HERE
            self.increment_progress()

            self.set_progress_message(f"T {time} : Cleaning cell complex")
            faces_to_remove = [f for f in topomesh.wisps(2) if topomesh.nb_borders(2, f)<3]
            for f in faces_to_remove:
                topomesh.remove_wisp(2, f)

            topomesh = clean_topomesh(topomesh, clean_properties=True, degree=2)
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating cell complex")
            for degree in [1, 2, 3]:
                compute_topomesh_property(topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))
            self.increment_progress()

            self.topomesh[time] = topomesh
