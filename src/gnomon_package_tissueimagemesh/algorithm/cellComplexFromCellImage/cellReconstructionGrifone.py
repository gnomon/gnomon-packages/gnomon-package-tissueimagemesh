from dtkcore import d_bool, d_int, d_real

import gnomon.core
import numpy as np

from gnomon.core import gnomonAbstractCellComplexFromCellImage

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.extraction import clean_topomesh

from timagetk_geometry.grifone.grifone import segmentation_topological_element_cell_topomesh

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellComplexOutput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="GRIFONE Reconstruction")
@cellImageInput('seg_img', data_plugin="gnomonCellImageDataTissueImage")
@cellComplexOutput('element_topomesh', data_plugin="gnomonCellComplexDataPropertyTopomesh")
class cellReconstructionGrifone(gnomonAbstractCellComplexFromCellImage):
    """Reconstruct a cell complex using Draco-Stem

    Note
    ----

    The documentation is not complete

    """

    def __init__(self):
        super().__init__()
        self.seg_img = {}
        self.element_topomesh = {}
        self.adjacency_series = {}

        self._parameters = {}
        self._parameters['dimension'] = d_int('Dimension', 2, 2, 3, "Dimension of the adjacency complex")
        self._parameters['resampling_voxelsize'] = d_real('Voxelsize', 0, 0, 3, 1, "Voxelsize used to resample the image (0 means no resampling)")
        self._parameters['triangulate'] = d_bool('Triangulate', False, "Whether to triangulate the interfaces of the resulting cell complex")
        self._parameters['fuse_vertices'] = d_bool('Fuse vertices', True, "Whether to fuse close disconnected vertices")

        self.__doc__ = segmentation_topological_element_cell_topomesh.__doc__

    def adjacencyComplex(self):
        return None

    def run(self):
        self.set_max_progress(2*len(self.seg_img))
        self.element_topomesh = {}

        for time in self.seg_img.keys():
            self.set_progress_message(f"T {time} : Extracting GRIFONE cell complex")
            seg_img = self.seg_img[time]

            resampling_voxelsize = None if self['resampling_voxelsize']==0 else self['resampling_voxelsize']
            element_topomesh = segmentation_topological_element_cell_topomesh(
                seg_img,
                dimension=self['dimension'],
                resampling_voxelsize=resampling_voxelsize,
                polygonal=not(self['triangulate']),
                fuse_vertices=self['fuse_vertices']
            )
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating cell complex")
            for degree in [1, 2, 3]:
                compute_topomesh_property(element_topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = element_topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    element_topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))
            self.increment_progress()

            self.element_topomesh[time] = element_topomesh
