# ChangeLog

## version 1.0.0 - 2024-03-27
* gnomon 1.0.0
* progress bar + message
* more operation on mesh


## version 0.3.0 - 2023-07-10
* gnomon 0.81.0
* new cell layer/curvature plugin
* enhance image surface plugin
